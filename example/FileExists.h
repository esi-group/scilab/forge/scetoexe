//==============================================================================
/* Allan CORNET - 2010 */
//==============================================================================
#ifndef __FILEEXISTS_H__
#define __FILEEXISTS_H__
#if __cplusplus
extern "C" {
#endif
BOOL FileExists(const char *filename);
#if __cplusplus
}
#endif
#endif /* __FILEEXISTS_H__ */
//==============================================================================
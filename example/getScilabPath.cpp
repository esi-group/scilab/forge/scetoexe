//==============================================================================
/* Allan CORNET - 2010 */
//==============================================================================
#include <windows.h>
#include <string>
//==============================================================================
const static char* gWin32SubKey = "Software\\Scilab";
const static char* gWin64SubKey = "Software\\Wow6432Node\\Scilab";
//==============================================================================
typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
static LPFN_ISWOW64PROCESS fnIsWow64Process = NULL;
static BOOL firstIsWow64Call = TRUE;
//==============================================================================
static BOOL IsWow64(void);
//==============================================================================
std::string getScilabPath(void)
{
    const char *subKey = NULL;
    HKEY hKeyScilab, hKeyCurrentScilab;
    DWORD dwType = 0, dwBuffSize = 0;
    std::string strScilabPath("");

    //------------------------------------------------------------------------
    //	Read registry values for last Scilab install directory.
    //  NOTE: 64-bit Windows code is currently untested.
    //
    //  We are not checking for errors in reading the registry keys.
    //  The worst that happens is that we don't have a valid directory
    //  for the Scilab path and LoadLibraryEx will later on.
    //------------------------------------------------------------------------
    if (IsWow64())		// Use Win32 or Win64 registry key
    {
        subKey = gWin64SubKey;
    }
    else
    {
        subKey = gWin32SubKey;
    }

    // Open Scilab registry key
    if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, subKey, 0, KEY_READ, &hKeyScilab) ==  ERROR_SUCCESS)
    {
        char *last_install = NULL;
        char *install_dir = NULL;

        if (RegQueryValueEx(hKeyScilab, "LASTINSTALL", NULL, &dwType, NULL, &dwBuffSize) != ERROR_SUCCESS)
        {
            return strScilabPath;
        }

        try
        {
            last_install = new char[dwBuffSize];
            if (last_install == NULL)
            {
                return strScilabPath;
            }

            // Query value of last install
            if (RegQueryValueEx(hKeyScilab, "LASTINSTALL", NULL, &dwType, (LPBYTE)last_install, &dwBuffSize) != ERROR_SUCCESS)
            {
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Open last install registry key
            if (RegOpenKeyEx(hKeyScilab, last_install, 0, KEY_READ, &hKeyCurrentScilab) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Query Scilab path length
            if (RegQueryValueEx(hKeyCurrentScilab, "SCIPATH", NULL, &dwType, NULL, &dwBuffSize) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            install_dir = new char[dwBuffSize];
            if (install_dir == NULL)
            {
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Query value of Scilab path
            if (RegQueryValueEx(hKeyCurrentScilab, "SCIPATH", NULL, &dwType, (LPBYTE)install_dir, &dwBuffSize) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                RegCloseKey(hKeyCurrentScilab);
                if (install_dir) delete [] install_dir;
                if (last_install) delete [] last_install;
                return strScilabPath;
            }
            else
            {
                RegCloseKey(hKeyScilab);
                RegCloseKey(hKeyCurrentScilab);
                if (last_install) delete [] last_install;
                strScilabPath.assign(install_dir);
                if (install_dir) delete [] install_dir;
            }
        }
        catch (std::bad_alloc&)
        {
        }
    }
    return strScilabPath;
}
//==============================================================================
static BOOL IsWow64(void)
{
    BOOL bIsWow64 = FALSE;

    if (firstIsWow64Call)
    {
        if (fnIsWow64Process == NULL)
        {
            fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle("kernel32"), "IsWow64Process");
        }
        firstIsWow64Call = FALSE;
    }

    if (fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64)) return FALSE;
    }
    return bIsWow64;
}
//==============================================================================

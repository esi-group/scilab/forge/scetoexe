//==============================================================================
// Allan CORNET - 2010
//==============================================================================
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include "helpers_utils.h"
#include "file_embedded.h"
#include "scilabWrap.h"
#include "FileExists.h"
//==============================================================================
static char *TMPDIR = NULL;
//==============================================================================
static int expandFileEmbedded(const char* directory);
static int deleteFileEmbedded(const char* directory);
static void functionExit(void);
//==============================================================================
int scetoexe_Initialize(void)
{
    if (loadScilabDlls() == FALSE)
    {
        fprintf(stderr, "Error: CAN NOT LOAD SCILAB DLLS.\n");
        return -1;
    }

    if (loadSymbols() == FALSE)
    {
        freeScilabDlls();
        fprintf(stderr, "Error: CAN NOT LOAD SYMBOLS SCILAB.\n");
        return -2;
    }

    atexit(functionExit);


    if (callScilabOpen(NULL, TRUE, NULL, 0))
    {
        fprintf(stderr, "Error while calling StartScilab\n");
        return -3;
    }

    TMPDIR = getTMPDIR();
    if (TMPDIR == NULL)
    {
        fprintf(stderr, "Error while calling StartScilab\n");
        scetoexe_Finish();
        return -3;
    }

    if (expandFileEmbedded(TMPDIR))
    {
        fprintf(stderr, "Error: CAN NOT EXPAND FILE.\n");
        scetoexe_Finish();
        return -4;
    }

    return 0;
}
//==============================================================================
int scetoexe_ExecFile(void)
{
    std::string filenameToExec = std::string(TMPDIR) +  std::string("/") + std::string(FILENAME);
    return fileExec(filenameToExec.c_str());
}
//==============================================================================
int scetoexe_Finish(void)
{
    deleteFileEmbedded(TMPDIR);

    if (terminateScilab(NULL) == FALSE) 
    {
        freeSymbols(TRUE);
        fprintf(stderr, "Error while calling TerminateScilab\n");
        return -3;
    }

    freeSymbols(TRUE);

    return 0;
}
//==============================================================================
static void functionExit (void)
{
    deleteFileEmbedded(TMPDIR);
    if (isSymbolsLoaded()) freeSymbols(TRUE);
    if (TMPDIR)
    {
        free(TMPDIR);
        TMPDIR = NULL;
    }
}
//==============================================================================
static int expandFileEmbedded(const char* directory)
{
    int ierr = -1;
    if (directory == NULL) return -1;
    std::string fullfilename = std::string(directory) + std::string("/") + std::string(FILENAME);

    FILE *fpOut = fopen(fullfilename.c_str(), "wt");
    if (fpOut)
    {
        fprintf(fpOut, "%s", FILENAME_EMBEDDED);
        fclose(fpOut);
        ierr = 0;
    }

    return ierr;
}
//==============================================================================
static int deleteFileEmbedded(const char* directory)
{
    if (directory == NULL) return -1;
    std::string fullfilename = std::string(directory) + std::string("/") + std::string(FILENAME);

    if (FileExists(fullfilename.c_str()))
    {
        if (deleteafile((char*)fullfilename.c_str())) return 0;
    }
    return -1;
}
//==============================================================================

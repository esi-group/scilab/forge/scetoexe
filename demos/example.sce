//
// Allan CORNET - 2010
//
// example scetoexe
// scetoexe_generate(scetoexe_getRootPath() + "/demos/example.sce", scetoexe_getRootPath() + "/example")

clear
lines(0);

disp('Hello from a .sce");

A = ones(5, 3);
disp("A = ");
disp(A);
B = rand(5, 3);
disp("B = ");
disp(B);
C = A * B';
disp("C = A * B''");
disp("C = ");
disp(C);

beep();
input("Press RETURN", "s");


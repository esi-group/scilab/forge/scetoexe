//==============================================================================
/* Allan CORNET - 2010 */
//==============================================================================
#include <Windows.h>
#include "FileExists.h"
//==============================================================================
BOOL FileExists(const char *filename)
{
    if (filename)
    {
        WIN32_FIND_DATA FindFileData;
        HANDLE handle = FindFirstFile (filename, &FindFileData);
        if (handle != INVALID_HANDLE_VALUE)
        {
            FindClose (handle);
            return TRUE;
        }
    }
    return FALSE;
}
//==============================================================================
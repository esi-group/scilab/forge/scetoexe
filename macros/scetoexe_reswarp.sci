// Allan CORNET - 2010

function bOK = scetoexe_reswarp(filenameSrc, filenameDest, options)
 bOK = %F;
 [lhs, rhs] = argn(0);
 if rhs == 2 then
   options = "-r FILENAME_EMBEDDED -nc -z";
 end
 reswarpPath = scetoexe_getRootPath() + 'bin' + filesep() + 'reswarp';
 
 if getos() == "Windows" then
   reswarpPath = scetoexe_getRootPath() + 'bin' + filesep() + 'reswarp';
   reswarpPath = reswarpPath + ".exe";
 else
   reswarpPath = 'reswarp';
 end
 
 commandLine = reswarpPath + " " + options + " -o """ + filenameDest + """ """ + filenameSrc + """";
 [rep, stat] = unix_g(commandLine);
 bOK = (stat == 0);
endfunction

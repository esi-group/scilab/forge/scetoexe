// Allan CORNET - 2010

function p = scetoexe_getRootPath()
  [m, p] = libraryinfo('scetoexelib');
  p = fullpath(p + '../');
endfunction